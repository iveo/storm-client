<?php
namespace Storm\Security;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Key;

/**
 * Class Encrypt
 *
 * @package Storm\Security
 */
class Encrypt
{
    /**
     * @var
     */
    protected $key;


    /**
     * Encrypt constructor.
     * @param $key
     */
    public function __construct($key)
    {
        $this->key = Key::loadFromAsciiSafeString($key);
    }

    /**
     * @param $message
     *
     * @return bool|string
     */
    public function encrypt($message)
    {
        try {
            $encrypted = Crypto::Encrypt($message, $this->key);
        } catch (\Exception $e) {
            $encrypted = false;
        }
        return $encrypted;
    }

    /**
     * @param $message
     *
     * @return bool|string
     */
    public function decrypt($message)
    {
        try {
            $decrypted = Crypto::Decrypt($message, $this->key);
        } catch (\Exception $e) {
            $decrypted = false;
        }
        return $decrypted;
    }

    /**
     * @return Key
     * @throws \Defuse\Crypto\Exception\EnvironmentIsBrokenException
     */
    public static function generateKey()
    {
        return Key::createNewRandomKey();
    }
}