<?php


namespace Storm\Expose;


use Wsdl2PhpGenerator\ComplexType;
use Wsdl2PhpGenerator\Filter\FilterFactory;
use Wsdl2PhpGenerator\Generator;

class ExposeGenerator extends Generator
{
    protected function savePhp()
    {
        $factory = new FilterFactory();
        $filter = $factory->create($this->config);
        $filteredService = $filter->filter($this->service);
        $service = $filteredService->getClass();
        $filteredTypes = $filteredService->getTypes();
        if ($service == null) {
            throw new \Exception('No service loaded');
        }

        $exposePhp = ExposePhp::instance();
        foreach ($filteredTypes as $type) {
            if($type instanceof ComplexType) {
                $exposePhp->add(new ExposeClass($type->getMembers(),[],"Storm\\Model",$type->getIdentifier()));
            }
        }

    }

}