<?php


namespace Storm\Expose;


use Wsdl2PhpGenerator\Variable;

class ExposeClass
{
    /**
     * @var array|Variable
     */
    protected $vars;
    protected $methods;
    protected $namespace;
    protected $class;

    /**
     * ExposeClass constructor.
     * @param $vars
     * @param $methods
     * @param $namespace
     * @param $class
     */
    public function __construct($vars, $methods, $namespace, $class)
    {
        $this->vars = $vars;
        $this->methods = $methods;
        $this->namespace = $namespace;
        $this->class = $class;
    }

    public function data()
    {
        $string = "namespace {$this->namespace}{" . PHP_EOL;
        $string .= "/**" . PHP_EOL;
        $string .= " * {$this->namespace}\\{$this->class}" . PHP_EOL;
        foreach ($this->vars as $phpVariable) {
            /**
             * @var $phpVariable Variable;
             */
            $type = $phpVariable->getType();
            $variable = $phpVariable->getName();
            $string .= " * @property $type \$$variable" . PHP_EOL;
        }
        $string .= " */" . PHP_EOL;
        $string .= " class {$this->class} {}" . PHP_EOL;
        $string .= "}" . PHP_EOL;
        return $string;
    }
    public function className() {
        return $this->class;
    }
}