<?php


namespace Storm\Expose;


use Storm\Util\Str;

class ExposeService
{
    /**
     * @var array
     */
    protected $service;
    protected $name;

    /**
     * ExposeService constructor.
     * @param array $service
     */
    public function __construct(array $service, $name)
    {
        $this->service = $service;
        $this->name = $name;
    }

    public function buildAnnotations()
    {
        $data = "namespace Storm\\Proxy{" . PHP_EOL;
        $useTypes = [
        ];
        foreach ($this->service['Operations'] as $operation) {
            $useTypes[] = $operation['Returns'];
            foreach ($operation['Parameters'] as $parameter) {
                if (isset($parameter['Type'])) {
                    $useTypes[] = $parameter['Type'];
                }
            }
        }
        $useTypes = array_unique(array_diff($useTypes, [
            "int",
            "int[]",
            "string",
            "string",
            "array",
            "Guid",
            "bool",
            "void"
        ]));
        $useTypes[] = "StormItem";
        foreach ($useTypes as $use) {
            if (!Str::contains(".", $use)) {
                $data .= "use Storm\\Model\\$use;" . PHP_EOL;
            }
        }
        $data .= "/**" . PHP_EOL;
        $data .= " * Storm\\Proxy\\{$this->name}Proxy" . PHP_EOL;
        foreach ($this->service['Operations'] as $operation) {

            $params = [];
            foreach ($operation['Parameters'] as $parameter) {
                $type = "";
                if (isset($parameter['Type'])) {
                    $type = $parameter['Type'];
                }
                if (isset($parameter['Name'])) {
                    if (Str::contains("[]", $type)) {
                        $type = "array";
                    }
                    if (Str::contains(".", $type)) {
                        $type = "StormItem";
                    }
                    $params[] = "$type \${$parameter['Name']}";
                }
            }
            $return = $operation['Returns'];
            if (Str::contains("[]", $return)) {
                $return = "array";
            }
            if (Str::contains(".", $return)) {
                $return = "StormItem";
            }
            $description = $operation['Documentation'];
            $params = implode(',', $params);
            $data .= " * @method $return {$operation['Id']}($params) $description" . PHP_EOL;
            $data .= " * @method $return batch{$operation['Id']}($params) $description" . PHP_EOL;
        }
        $data .= " */" . PHP_EOL;
        $data .= " class {$this->name}Proxy extends AbstractProxy {" . PHP_EOL;
        $data .= " }" . PHP_EOL;
        $data .= "}" . PHP_EOL;
        $path = Str::trailingSlashIt(STORM_ROOT) . "/storm-{$this->name}.php";

        return $data;
    }
}