<?php


namespace Storm\Expose;


use Storm\StormClient;
use Storm\Util\Str;

class ExposePhp
{
    protected $classes;
    /**
     * @var ExposePhp
     */
    protected static $instance;

    /**
     * @return ExposePhp
     */
    public static function instance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    public function add(ExposeClass $class)
    {
        $this->classes[] = $class;
    }

    public function save($path = "")
    {
        if (empty($path)) {
            $path = StormClient::self()->exposePath() . "storm-classes.php";
        }
        $names = [];
        $data = "<?php " . PHP_EOL;
        foreach ($this->classes as $class) {
            if (!in_array($class->className(), $names)) {
                $data .= $class->data();
                $names[] = $class->className();
            }
        }
        file_put_contents($path, $data);
    }
}