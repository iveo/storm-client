<?php


namespace Storm\Expose;


class Wsdl
{
    protected $data;
    protected $name;
    protected $path;

    /**
     * Wsdl constructor.
     * @param $data
     * @param $name
     * @param $path
     */
    public function __construct($data, $name, $path)
    {
        $this->data = $data;
        $this->name = $name;
        $this->path = $path;
    }

    public function save()
    {
        if(!is_dir($this->trailingSlashIt($this->path))) {
            mkdir($this->trailingSlashIt($this->path));
        }
        file_put_contents($this->path(),$this->data);
    }

    public function path()
    {
        return $this->trailingSlashIt($this->path) . $this->name;
    }
    private function trailingSlashIt($string) {
        return rtrim( $string, '/\\' ) . "/";
    }
}